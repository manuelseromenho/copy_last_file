import glob
import shutil

"""
Script to replace the file "continue.sav" with the last saved file.
"""

def copy_file(source, destination):
	print("\n\n.......Copying.......\n{} \n\t\/ \n{}".format(source, destination))
	source_file = source
	destination_file = destination
	shutil.copy(source_file, destination_file)
	print(".......Copy..is...done...argh!!!....")

def check_last_file():
	glob_files_list = glob.iglob('* ([0-9]*).sav')
	latest_file = ""
	digit = 0
	for item in glob_files_list:
		temp_digit = int(''.join(filter(str.isdigit, item)))
		if temp_digit > digit:
			digit = temp_digit
			latest_file = item
	return latest_file

copy_file(check_last_file(), "continue.sav")





# input("\n\nPlease Press Enter to Exit")

# Check the if exists files with the name "continue" plus a number
# for item in glob.iglob('continue[0-9]*.sav'):
# 	print(item)

# Check last file by time
# import os
# latest_file = max(glob_files_list,key=os.path.getctime)
# print(latest_file)